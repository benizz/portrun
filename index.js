// [START all]
try { require('@google-cloud/debug-agent').start(); }
catch (e) {}
const Compute = require('@google-cloud/compute');
const fs = require('fs');
const http = require('http');
const https = require('https');

process.env.datelast = Date.now();


var pvm = {
    'nom':process.env.nom || "vm-plex-boys",
    'zone':process.env.zone || "europe-west1-d",
    
};

var tempina = parseInt(process.env.time) || 1200000;

var plex = {
    'ip' : process.env.ip || "35.187.187.155",
    'port': process.env.port || "32400"
};



(async () => {
var compute = new Compute({
  projectId: "model-envelope-218112",
  keyFilename: "key.json"
});



const zone = compute.zone(pvm.zone);
const vm = zone.vm(pvm.nom);
var stats = true;
vm.exists(function(err, exists) {if(!exists ) { console.error("VM not exists");process.exit(0);}});
})();

(async () => {
var shttp = http.createServer((req,res) => {

    if(req.url == "/checkvmstatus.run") {
      if((Date.now() - process.env.datelast) > tempina) {vm.stop(); stats=false}
      res.writeHead(200,{ 'Content-Type': 'text/plain' });
      res.end();
      return;
    }

    res.writeHead(301,{'location':('https://' + ( req.headers.host || req.socket.localAddress ) + ":" + plex.port)});
    res.end();
    return;
});

shttp.listen(80, e => {if(e) console.error(e); else console.log("http OK");});
})();
var privateKey  = fs.readFileSync(__dirname + '/server.key', 'utf8');
var certificate = fs.readFileSync(__dirname + '/server.cert', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var shttps = https.createServer(credentials,(req,res) => {

  process.env.datelast = Date.now();
    let body = '';
    if(req.method == 'POST') {
      
      req.on('data',c => {
        body += c.toString();
      })
    }

    var succes = true;
    (async () => {
        vm.start(e => {if(e) {/*res.writeHead(500); res.write('error : '+JSON.stringify(e)); res.end(); succes = false;*/}});
    })();
    if(!succes) return;
    stats = true;

    vm.waitFor("RUNNING",(err) => {
        if(!err) {
            var err, forwardReq, options;
            try {
      
      var head = req.headers;
      head.host = plex.ip + ":" + plex.port;

      var options = {
        host: plex.ip,
        port: plex.port, 
        path: req.url, 
        method: req.method,
        family:4,
        rejectUnauthorized:false,
        //query: req.query,
        headers: head

      };
      
      forwardReq = https.request(options, function(forwardRes) {

        res.writeHead(forwardRes.statusCode, forwardRes.headers);
        forwardRes.on('data', function(chunk) {
          return res.write(chunk);
        });
        
        forwardRes.on('close', function() {
          return res.end();
        });
        
        return forwardRes.on('end', function() {
          return res.end();
        });
  
      }).on('error', function(err) {
        res.writeHead(503, {
          'Content-Type': 'text/plain'
        });
        console.error(err);
        res.write("Service currently unvailable");
        return res.end();
      });
  
      
      forwardReq.write(JSON.stringify(body || {}));
      return forwardReq.end();
    } catch (_error) {
      console.error('https request to service raised: ', _error);
      try {
        res.writeHead(503,{
          'Content-Type': 'text/plain'
        });
        res.write("Service currently unvailable");
        return res.end();
      } catch (_error) {
        err = _error;
        return console.error('Could not send error response: ', err);
      }
    }
        }
    });



    
});

/*
(async () => {
  function loop() {
    setTimeout(() => {
      if((Date.now() - process.env.datelast) > tempina && stats) {vm.stop(); stats=false}
      loop();
    },5000);
  }
})()*/


/*shttps.listen(plex.port);
shttp.listen(80)*/
shttps.listen(plex.port, e => {if(e) console.error(e); else console.log("https OK");});

// [END all]
