'use strict';

var _stringify = require('babel-runtime/core-js/json/stringify');

var _stringify2 = _interopRequireDefault(_stringify);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fs = require('fs');
var http = require('http');
var https = require('https');

process.env.datelast = Date.now();

var pvm = {
  'nom': process.env.nom || "vm-plex-boys",
  'zone': process.env.zone || "europe-west1-d"

};

var tempina = parseInt(process.env.time) || 1200000;

var plex = {
  'ip': process.env.ip || "35.187.187.155",
  'port': process.env.port || "32400"
};

(0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee() {
  var compute, zone, vm, stats;
  return _regenerator2.default.wrap(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          compute = new Compute({
            projectId: "model-envelope-218112",
            keyFilename: "key.json"
          });
          zone = compute.zone(pvm.zone);
          vm = zone.vm(pvm.nom);
          stats = true;

          vm.exists(function (err, exists) {
            if (!exists) {
              console.error("VM not exists");process.exit(0);
            }
          });

        case 5:
        case 'end':
          return _context.stop();
      }
    }
  }, _callee, undefined);
}))();

(0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2() {
  var shttp;
  return _regenerator2.default.wrap(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          shttp = http.createServer(function (req, res) {

            if (req.url == "/checkvmstatus.run") {
              if (Date.now() - process.env.datelast > tempina) {
                vm.stop();stats = false;
              }
              res.writeHead(200, { 'Content-Type': 'text/plain' });
              res.end();
              return;
            }

            res.writeHead(301, { 'location': 'https://' + (req.headers.host || req.socket.localAddress) + ":" + plex.port });
            res.end();
            return;
          });


          shttp.listen(80, function (e) {
            if (e) console.error(e);else console.log("http OK");
          });

        case 2:
        case 'end':
          return _context2.stop();
      }
    }
  }, _callee2, undefined);
}))();
var privateKey = fs.readFileSync(__dirname + '/server.key', 'utf8');
var certificate = fs.readFileSync(__dirname + '/server.cert', 'utf8');

var credentials = { key: privateKey, cert: certificate };

var shttps = https.createServer(credentials, function (req, res) {

  process.env.datelast = Date.now();
  var body = '';
  if (req.method == 'POST') {

    req.on('data', function (c) {
      body += c.toString();
    });
  }

  var succes = true;
  (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3() {
    return _regenerator2.default.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            vm.start(function (e) {
              if (e) {/*res.writeHead(500); res.write('error : '+JSON.stringify(e)); res.end(); succes = false;*/}
            });

          case 1:
          case 'end':
            return _context3.stop();
        }
      }
    }, _callee3, undefined);
  }))();
  if (!succes) return;
  stats = true;

  vm.waitFor("RUNNING", function (err) {
    if (!err) {
      var err, forwardReq, options;
      try {

        var head = req.headers;
        head.host = plex.ip + ":" + plex.port;

        var options = {
          host: plex.ip,
          port: plex.port,
          path: req.url,
          method: req.method,
          family: 4,
          rejectUnauthorized: false,
          //query: req.query,
          headers: head

        };

        forwardReq = https.request(options, function (forwardRes) {

          res.writeHead(forwardRes.statusCode, forwardRes.headers);
          forwardRes.on('data', function (chunk) {
            return res.write(chunk);
          });

          forwardRes.on('close', function () {
            return res.end();
          });

          return forwardRes.on('end', function () {
            return res.end();
          });
        }).on('error', function (err) {
          res.writeHead(503, {
            'Content-Type': 'text/plain'
          });
          console.error(err);
          res.write("Service currently unvailable");
          return res.end();
        });

        forwardReq.write((0, _stringify2.default)(body || {}));
        return forwardReq.end();
      } catch (_error) {
        console.error('https request to service raised: ', _error);
        try {
          res.writeHead(503, {
            'Content-Type': 'text/plain'
          });
          res.write("Service currently unvailable");
          return res.end();
        } catch (_error) {
          err = _error;
          return console.error('Could not send error response: ', err);
        }
      }
    }
  });
});

/*
(async () => {
  function loop() {
    setTimeout(() => {
      if((Date.now() - process.env.datelast) > tempina && stats) {vm.stop(); stats=false}
      loop();
    },5000);
  }
})()*/

/*shttps.listen(plex.port);
shttp.listen(80)*/
shttps.listen(plex.port, function (e) {
  if (e) console.error(e);else console.log("https OK");
});